﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Moves;

public class Controller : MonoBehaviour
{
    public MoveManager moveManager;
    public MoveVariables moveVars;
    public Conditionals conditionals;

    public float Gravity = 15f;

    void Awake()
    {
        moveManager.Initialize();
    }

    void Start()
    {
        moveManager.Enable<ManualControl>();
    }

    void Update()
    {
        #region Key Input
        //EKey
        if (Input.GetKeyDown(KeyCode.E))
        {
            moveManager.Invoke<TestMove>();
        }

        //WKey
        if (Input.GetKey(KeyCode.W))
        {
            //moveManager.Invoke<TestMoveTwo>();
        }

        //spaceKey
        if (Input.GetKey(KeyCode.Space))
        {
            moveManager.Invoke<Jump>();
        }
        #endregion

        moveManager.Manual_Update();

        #region Gravity
        if (conditionals.IsFalling)
        {
            if (transform.position.y >= 0)
            {
                moveVars.MoveVector = transform.position + (Vector3.down * Gravity);
            }

            else
            {
                conditionals.IsFalling = false;
                moveManager.Enable<Jump>();
            }
        }
        #endregion
    }

    void LateUpdate()
    {
        moveManager.Manual_LateUpdate();
        //Debug.Log("moveVars.MoveVector " + moveVars.MoveVector);
        transform.Translate(moveVars.MoveVector * Time.deltaTime);

        //Debug.Log(moveVars.MoveVector);
        moveVars.MoveVector = Vector3.zero;
        //Debug.Log(moveVars.MoveVector);
    }
}
