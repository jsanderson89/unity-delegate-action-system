﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{

    #region State Class
    public class State
    {
        #region Variables
        protected Enum _ID;
        public Enum ID
        {
            get
            {
                return _ID;
            }
        }
        public List<Enum> _ValidTransitions = new List<Enum>();
        public State(Enum idOfState)
        {
            _ID = idOfState;
        }
        #endregion

        #region Lifecycle
        public Action Enter_State = DoNothing;
        public Action Update_State = DoNothing;
        public Action FixedUpdate_State = DoNothing;
        public Action LateUpdate_State = DoNothing;
        public Action Exit_State = DoNothing;
        #endregion

        public void AddEnumToTransitions(Enum stateToAdd)
        {
            //Debug.Log("Add Enum entered!");
            if (_ValidTransitions.Contains(stateToAdd) != true)
            {
                _ValidTransitions.Add(stateToAdd);
                //Debug.Log("Added transition to " + _ID + " " + stateToAdd);
            }
        }
        public void RemoveEnumFromTransitions(Enum stateToRemove)
        {
            if (_ValidTransitions.Contains(stateToRemove) != true)
            {
                _ValidTransitions.Remove(stateToRemove);
            }

            else
            {
                //did not contain
            }

        }
    }
    #endregion

    #region Variables
    #region Variables - State Control
    protected State _CurrentState;
    protected State _PreviousState;
    protected Dictionary<Enum, State> _StateCache;
    #endregion

    #region Variables - Control Bool
    [HideInInspector]
    public bool init = false;
    #endregion
    #endregion

    #region Unity Lifecycle
    // Use this for initialization
    void Awake()
    {
        //Debug.Log("StateMachine_Awake");
        Initialize_State_OnAwake();
    }

    void Start()
    {
        //Debug.Log("StateMachine_Start");
        Initialize_State_OnStart();
    }
    #endregion

    #region Initialization
    protected virtual void Initialize_State_OnAwake()
    {
    }

    protected virtual void Initialize_State_OnStart()
    {
    }

    #endregion

    #region State Creation and Control
    //when you call this, T will be the Enumerator listing all possible states
    protected void InitializeMachine<T>(Enum initialState)
    {
        //Debug.Log("Initialize Machine");
        if (this.init == false)
        {
            //Debug.Log("Initialize Machine init entered");
            Array stateIDs = Enum.GetValues(typeof(T));
            _StateCache = new Dictionary<Enum, State>();

            //add all states to cache
            foreach (Enum id in stateIDs)
            {
                State stateToAdd = CreateState(id);
                _StateCache.Add(id, stateToAdd);
            }


            _CurrentState = _StateCache[initialState];
            this.init = true;
        }
    }

    protected void ChangeState(Enum changeState)
    {
        //Debug.Log("ChangeState entered");
        if (this.init == true)
        {
            //Debug.Log("Initialized True");
            if (_CurrentState._ValidTransitions.Contains(changeState))
            {
                //Debug.Log("Valid Transition");
                _CurrentState.Exit_State();
                _PreviousState = _CurrentState;
                _CurrentState = _StateCache[changeState];
                _CurrentState.Enter_State();
            }
            else
            {
                Debug.LogError("Transition not valid!");
            }
        }
    }

    //used for Pausing; does not call Exit State.
    protected void InterruptState(Enum changeState)
    {
        //Debug.Log("ChangeState entered");
        if (this.init == true)
        {
            //Debug.Log("Initialized True");
            if (_CurrentState._ValidTransitions.Contains(changeState))
            {
                //Debug.Log("Valid Transition");
                _PreviousState = _CurrentState;
                _CurrentState = _StateCache[changeState];
                _CurrentState.Enter_State();
            }
            else
            {
                Debug.LogError("Transition not valid!");
            }
        }
    }

    //used for Pausing; does not call Enter State.
    protected void ResumeState()
    {
        Enum changeState = _PreviousState.ID;

        //Debug.Log("ChangeState entered");
        if (this.init == true)
        {
            //Debug.Log("Initialized True");
            if (_CurrentState._ValidTransitions.Contains(changeState))
            {
                //Debug.Log("Valid Transition");
                _CurrentState.Exit_State();
                _PreviousState = _CurrentState;
                _CurrentState = _StateCache[changeState];
            }
            else
            {
                Debug.LogError("Transition not valid!");
            }
        }
    }

    //overwritten in the child classes = this contains the initialization of each class, including the initial valid statechanges
    public virtual State CreateState(Enum inputState)
    {
        return null;
    }
    #endregion

    #region Default States
    static void DoNothing() { }
    #endregion

}
