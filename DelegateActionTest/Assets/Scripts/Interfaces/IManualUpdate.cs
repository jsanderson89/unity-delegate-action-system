﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interfaces
{
    interface IManualUpdate
    {
        void Manual_FixedUpdate();
        void Manual_Update();
        void Manual_LateUpdate();


        //Example Usage
        /*
            public void Manual_FixedUpdate()
            {
                if (this.init)
                {
                    _CurrentState.FixedUpdate_State();
                }
            }

            public void Manual_Update()
            {
                //Debug.Log(_CurrentState.ID);
                if (this.init)
                {
                    _CurrentState.Update_State();
                }
            }

            public void Manual_LateUpdate()
            {
                //Debug.Log(_CurrentState.ID);
                if (this.init)
                {
                    _CurrentState.LateUpdate_State();
                }
            }
         */
    }
}