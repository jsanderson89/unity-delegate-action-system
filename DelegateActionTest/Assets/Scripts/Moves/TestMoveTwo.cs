﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Moves
{
    public class TestMoveTwo : Move
    {
        #region Initialization - Manual Initialization
        public override void Initialize()
        {
            InitializeMachine<MoveState>(MoveState.Listen);
        }
        #endregion

        #region Active
        protected override void Active_OnEnter()
        {
            Debug.Log("OnEnter_TestMoveTWO");
            ChangeState(MoveState.Listen);
        }
        #endregion

        public override void Invoke()
        {
            if (_CurrentState.ID.Equals(MoveState.Listen))
            {
                ChangeState(MoveState.Active);
            }

        }
    }
}
