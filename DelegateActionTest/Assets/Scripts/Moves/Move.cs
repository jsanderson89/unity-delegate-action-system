﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Interfaces;
using UnityEngine;

namespace Moves
{
    public class Move : StateMachine, IManualUpdate
    {

        public enum MoveState
        {
            DoNothing,
            Listen,
            Active,
            Disabled
        }

        #region Initialization
        #region Initialization - State initialization
        protected sealed override void Initialize_State_OnAwake()
        {
            Initialize_Move_OnAwake();
        }

        protected sealed override void Initialize_State_OnStart()
        {
            Initialize_Move_OnStart();
        }
        #endregion
        #region Initialization - Move Initlialization (Automatic)
        protected virtual void Initialize_Move_OnAwake()
        {
        }

        protected virtual void Initialize_Move_OnStart()
        {
            //initialize your machine HERE
        }

        #endregion
        #endregion

        #region Initialization - Manual Initialization
        /// <summary>
        /// The initial manual initialization function. States Machine should be initialized here.
        /// </summary>
        public virtual void Initialize() { }
        #endregion

        #region Manual Lifecycle
        public void Manual_FixedUpdate()
        {
            if (this.init)
            {
                _CurrentState.FixedUpdate_State();
            }
        }

        public void Manual_Update()
        {
            //Debug.Log(_CurrentState.ID);
            if (this.init)
            {
                _CurrentState.Update_State();
            }
        }

        public void Manual_LateUpdate()
        {
            //Debug.Log(_CurrentState.ID);
            if (this.init)
            {
                _CurrentState.LateUpdate_State();
            }
        }
        #endregion

        #region State Initialization
        public override State CreateState(Enum inputState)
        {
            //Debug.Log("CREATE STATE ENTERED");
            MoveState stateToMake = (MoveState)inputState;
            State returnState;

            switch (stateToMake)
            {
                #region DoNothing
                case MoveState.DoNothing:
                    returnState = new State(stateToMake);
                    returnState.AddEnumToTransitions(MoveState.Listen);
                    returnState.AddEnumToTransitions(MoveState.Active);
                    returnState.AddEnumToTransitions(MoveState.Disabled);
                    return returnState;
                #endregion

                //This is the LISTEN state. It is the default state during which the script Listens for the Invoke command.
                #region Listen
                case MoveState.Listen:
                    returnState = new State(stateToMake);
                    returnState.AddEnumToTransitions(MoveState.Active);
                    returnState.AddEnumToTransitions(MoveState.Disabled);

                    #region Listen Methods
                    returnState.Enter_State = Listen_OnEnter;
                    returnState.FixedUpdate_State = Listen_FixedUpdate;
                    returnState.Update_State = Listen_Update;
                    returnState.LateUpdate_State = Listen_LateUpdate;
                    returnState.Exit_State = Listen_OnExit;
                    #endregion

                    return returnState;
                #endregion

                //This is the ACTIVE state. It is the state during which the script is performing its function.
                #region Active
                case MoveState.Active:
                    returnState = new State(stateToMake);
                    returnState.AddEnumToTransitions(MoveState.Listen);
                    returnState.AddEnumToTransitions(MoveState.Disabled);

                    #region Active Methods
                    returnState.Enter_State = Active_OnEnter;
                    returnState.FixedUpdate_State = Active_FixedUpdate;
                    returnState.Update_State = Active_Update;
                    returnState.LateUpdate_State = Active_LateUpdate;
                    returnState.Exit_State = Active_OnExit;
                    #endregion

                    return returnState;
                #endregion

                //This is the Disabled state. It is the state in which the script cannot be called.
                #region Disabled
                case MoveState.Disabled:
                    returnState = new State(stateToMake);
                    returnState.AddEnumToTransitions(MoveState.Listen);
                    returnState.AddEnumToTransitions(MoveState.Active);

                    #region Disabled Methods
                    returnState.Enter_State = Disabled_OnEnter;
                    returnState.FixedUpdate_State = Disabled_FixedUpdate;
                    returnState.Update_State = Disabled_Update;
                    returnState.LateUpdate_State = Disabled_LateUpdate;
                    returnState.Exit_State = Disabled_OnExit;
                    #endregion
                    return returnState;
                #endregion

                default:
                    returnState = new State(stateToMake);
                    return returnState;
            }
        }
        #endregion

        #region State Function Delegates

        #region Listen


        protected virtual void Listen_OnEnter() { }
        protected virtual void Listen_FixedUpdate() { }
        protected virtual void Listen_Update() { }
        protected virtual void Listen_LateUpdate() { }
        protected virtual void Listen_OnExit() { }

        #endregion

        #region Active
        protected virtual void Active_OnEnter() { }
        protected virtual void Active_FixedUpdate() { }
        protected virtual void Active_Update() { }
        protected virtual void Active_LateUpdate() { }
        protected virtual void Active_OnExit() { }
        #endregion

        #region Disabled

        protected virtual void Disabled_OnEnter() { }
        protected virtual void Disabled_FixedUpdate() { }
        protected virtual void Disabled_Update() { }
        protected virtual void Disabled_LateUpdate() { }
        protected virtual void Disabled_OnExit() { }
        #endregion

        #endregion

        #region Invokers
        /// <summary>
        /// Invokes the Move, usually once. Generally will change state from "Listen" to "Active."
        /// </summary>
        public virtual void Invoke() { }

        /// <summary>
        /// Disenvokes the Move. Generally should change state from "Active" to "Listen."
        /// </summary>
        public virtual void Terminate() { }

        /// <summary>
        /// Disables the Move. Generally should change state from "Listen" or "Active" to "Disabled"
        /// </summary>
        /// <param name="interrupt">Whether or not to change state with "InterruptState."</param>
        public virtual void Disable(bool interrupt) { }

        /// <summary>
        /// Enables the Move. Generally should change state from "Disabled" to "Listen" or to "Active"
        /// </summary>
        public virtual void Enable() { }
        #endregion
    }
}
