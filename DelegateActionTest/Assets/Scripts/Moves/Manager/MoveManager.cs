﻿using System.Collections.Generic;
using Interfaces;
using UnityEngine;

namespace Moves
{
    public class MoveManager : MonoBehaviour, IManualUpdate
    {
        public List<Move> MoveList = new List<Move>();

        [HideInInspector]
        public bool Initialized = false;

        #region Initialization
        public void Initialize()
        {
            foreach (Move i in MoveList)
            {
                i.Initialize();
            }

            Initialized = true;
        }
        #endregion

        #region Manual Update
        public void Manual_FixedUpdate()
        {
            FixedUpdateMoves();
        }
        public void Manual_Update()
        {
            UpdateMoves();
        }
        public void Manual_LateUpdate()
        {
            LateUpdateMoves();
        }

        #region Manual Update Movelist
        protected void FixedUpdateMoves()
        {
            foreach (Move i in MoveList)
            {
                i.Manual_FixedUpdate();
            }
        }
        protected void UpdateMoves()
        {
            foreach (Move i in MoveList)
            {
                i.Manual_Update();
            }
        }
        protected void LateUpdateMoves()
        {
            foreach (Move i in MoveList)
            {
                i.Manual_LateUpdate();
            }
        }
        #endregion
        #endregion

        public MType GetMove<MType>() where MType : Move
        {
            return MoveList.Find(move => move is MType) as MType;
        }

        #region Control Commands
        public void Invoke<MType>() where MType : Move
        {
            if (Initialized)
            {
                MType move = GetMove<MType>();
                if (move.init)
                {
                    move.Invoke();
                }
            }
        }

        public void Terminate<MType>() where MType : Move
        {
            if (Initialized)
            {
                MType move = GetMove<MType>();
                if (move.init)
                {
                    move.Terminate();
                }
            }
        }

        public void Disable<MType>(bool interrupt) where MType : Move
        {
            if (Initialized)
            {
                MType move = GetMove<MType>();
                if (move.init)
                {
                    move.Disable(interrupt);
                }
            }
        }

        public void Enable<MType>() where MType : Move
        {
            if (Initialized)
            {
                MType move = GetMove<MType>();
                if (move.init)
                {
                    move.Enable();
                }
            }
        }
        #endregion
    }
}