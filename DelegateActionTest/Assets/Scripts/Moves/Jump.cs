﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Moves
{
    public class Jump : Move
    {
        public float JumpVal = 1f;
        public float MaxJumpHeight = 10f;

        public Conditionals _conditionals;
        public MoveVariables _moveVars;

        private Vector3 startPosition;

        #region Initialization - Manual Initialization
        public override void Initialize()
        {
            InitializeMachine<MoveState>(MoveState.Listen);
        }
        #endregion

        #region Active
        protected override void Active_OnEnter()
        {
            Debug.Log("Jump!");
            startPosition = _moveVars.Position;

            _conditionals.IsJumping = true;
        }

        protected override void Active_Update()
        {
            _moveVars.MoveVector += Vector3.up * JumpVal;

            //Debug.Log(_moveVars.MoveVector);

            if (_moveVars.Position.y >= (startPosition.y + MaxJumpHeight))
            {
                ChangeState(MoveState.Disabled);
            }
        }

        protected override void Active_OnExit()
        {
            _conditionals.IsJumping = false;
            _conditionals.IsFalling = true;
        }
        #endregion

        public override void Invoke()
        {
            if (_CurrentState.ID.Equals(MoveState.Listen))
            {
                ChangeState(MoveState.Active);
            }
        }

        public override void Enable()
        {
            if (_CurrentState.ID.Equals(MoveState.Disabled))
            {
                ChangeState(MoveState.Listen);
            }
        }
    }
}
