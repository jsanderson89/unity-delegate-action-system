﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Moves
{
    public class ManualControl : Move
    {
        public float Speed;
        public Conditionals _conditionals;
        public MoveVariables _moveVars;

        #region Initialization - Manual Initialization
        public override void Initialize()
        {
            InitializeMachine<MoveState>(MoveState.Disabled);
        }
        #endregion

        #region Active
        protected override void Active_Update()
        {
            if (Input.GetKey(KeyCode.W))
            {
                _moveVars.MoveVector += Vector3.forward * Speed;
            }

            if (Input.GetKey(KeyCode.D))
            {
                _moveVars.MoveVector += Vector3.right * Speed;
            }

            if (Input.GetKey(KeyCode.S))
            {
                _moveVars.MoveVector += Vector3.back * Speed;
            }

            if (Input.GetKey(KeyCode.A))
            {
                _moveVars.MoveVector += Vector3.left * Speed;
            }
        }
        #endregion

        public override void Enable()
        {
            if (_CurrentState.ID.Equals(MoveState.Disabled))
            {
                ChangeState(MoveState.Active);
            }
        }

        public override void Disable(bool interrupt)
        {
            if (_CurrentState.ID.Equals(MoveState.Active))
            {
                ChangeState(MoveState.Disabled);
            }
        }
    }
}
